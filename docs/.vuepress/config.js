module.exports = {
  title: 'GitLab Workshops',
  description: 'Everyone can contribute',
  dest:"public",
  body: [],
  themeConfig: {
    lastUpdated: 'Last Updated', // string | boolean
    sidebar: [
      '/',
      'WORKSHOPS'
    ]
  },
  plugins: []
}
