---
home: true
actionText: 👋 Go to the WorkShops list
actionLink: /WORKSHOPS
features:
- title: CI/CD
- title: Project Management
- title: And more ...
footer: MIT Licensed | Copyright © 2019-2020 GitLab
---

<gitlab-logo/>